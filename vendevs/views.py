from django.shortcuts import render_to_response
from django.views.generic import FormView
from forms import ContactForm
from django.core.urlresolvers import reverse_lazy


def home(request):
    return render_to_response('base.html')


class ContactView(FormView):
    template_name = "contact.html"
    form_class = ContactForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.send_email()

        return super(ContactView, self).form_valid(form)
