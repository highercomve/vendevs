from django.conf.urls import patterns, url, include
from devs.urls import urlpatterns as dev_urls
from views import home, ContactView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'vendevs.views.home', name='home'),
    # url(r'^vendevs/', include('vendevs.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^contact/', ContactView.as_view(), name="contact"),
    url(r'^$', home, name="home")
)

urlpatterns += dev_urls
