from django import forms
from django.core.mail import send_mail
from django.conf import settings


class ContactForm(forms.Form):
    name = forms.CharField(max_length=50, required=True)
    email = forms.EmailField(required=True)
    content = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        mail_content = "%s - %s\n\n\n%s" % (self.cleaned_data['name'], self.cleaned_data['email'], self.cleaned_data['content'])
        send_mail("Contacto de vendevs", mail_content, settings.CONTACT_EMAIL, [settings.CONTACT_EMAIL], fail_silently=False)
