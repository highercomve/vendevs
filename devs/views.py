from django.views.generic import ListView, FormView
from models import Developer
from forms import DeveloperForm
from django.core.urlresolvers import reverse_lazy


class DeveloperList(ListView):
    model = Developer


class DeveloperCreate(FormView):
    template_name = "devs/developer_form.html"
    form_class = DeveloperForm
    success_url = reverse_lazy('developer_list')

    def form_valid(self, form):
        form.save()

        return super(DeveloperCreate, self).form_valid(form)
