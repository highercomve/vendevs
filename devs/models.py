from django.db import models
from django.contrib.auth.models import AbstractUser
from django_thumbs.db.models import ImageWithThumbsField
from south.modelsinspector import add_introspection_rules
USER_THUMB_SIZES = ((125, 125), (200, 200))

add_introspection_rules(
    [([ImageWithThumbsField], [], {"sizes": ["sizes", {}]})],
    ["^django_thumbs\.db\.models\.ImageWithThumbsField"]
)


class Developer(AbstractUser):
    location = models.CharField(max_length=50, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    pict = ImageWithThumbsField(upload_to='developer_images/', sizes=USER_THUMB_SIZES, null=True, blank=True)
    show_email = models.BooleanField(default=True)
    twitter_handle = models.CharField(max_length=20, null=True, blank=True)
    interests = models.TextField(blank=True, null=True)
    project = models.CharField(max_length=200, null=True, blank=True)
    available = models.BooleanField(default=False)

    def get_full_name(self):
        return self.first_name + " " + self.last_name

    def get_short_name(self):
        return self.first_name

    def __unicode__(self):
        return self.username
