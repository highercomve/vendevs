from django.conf.urls import patterns, url
from views import DeveloperList, DeveloperCreate

urlpatterns = patterns(
    '',
    url(r'^devs/$', DeveloperList.as_view(), name="developer_list"),
    url(r'^devs/create$', DeveloperCreate.as_view(), name="developer_create")
)
